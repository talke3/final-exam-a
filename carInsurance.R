setwd("C:/Users/tal/Desktop/��� � - ������ ������/����� �/����� ��� ������ �����")
carinsurance.original <- read.csv("carInsurance_train.csv")
#------Package List------
library(ggplot2)
library(dplyr)
library(lubridate)
library(caTools)
library(tm)
library(ROSE)
library(rpart)
library(rpart.plot)
library(pROC)
library(randomForest)
#install.packages('rattle')
#library(rattle)

str(carinsurance.original)
carinsurance <- carinsurance.original
str(carinsurance)
#Target Feature Testing
table(carinsurance$CarInsurance)
ggplot(carinsurance, aes(CarInsurance)) + geom_bar()

#Q1 ----------------------------------------------------------------------------------------------
start <- hms(carinsurance$CallStart)
end <- hms(carinsurance$CallEnd)
carinsurance$CallDuration <- end-start
#carinsurance$CallDuration<-paste(hour(carinsurance$CallDuration),minute(carinsurance$CallDuration),second(carinsurance$CallDuration),sep=":")

carinsurance.new <- carinsurance
str(carinsurance.new)
#Select 10 features for the new data frame
ggplot(carinsurance.new, aes(Marital, fill = CarInsurance)) + geom_bar()
ggplot(carinsurance.new, aes(Education , fill = CarInsurance)) + geom_bar()
ggplot(carinsurance.new, aes(Balance , fill = CarInsurance)) + geom_bar()
ggplot(carinsurance.new, aes(Age, CarInsurance)) + geom_point()
ggplot(carinsurance.new, aes(Communication , fill = CarInsurance)) + geom_bar()
ggplot(carinsurance.new, aes(NoOfContacts, CarInsurance)) + geom_point()
ggplot(carinsurance.new, aes(CarLoan  , fill = CarInsurance)) + geom_bar()

carinsurance.new$Default <- NULL
carinsurance.new$PrevAttempts <- NULL
carinsurance.new$HHInsurance<- NULL
carinsurance.new$Id<- NULL
carinsurance.new$LastContactDay <- NULL
carinsurance.new$LastContactMonth<- NULL
carinsurance.new$Outcome<- NULL
carinsurance.new$DaysPassed <- NULL
carinsurance.new$CallStart<- NULL
carinsurance.new$CallEnd<- NULL
#Q2 ----------------------------------------------------------------------------------------------
str(carinsurance.new)
#Change features to factors
carinsurance.new$CarLoan <- as.factor(carinsurance.new$CarLoan)
#carinsurance.new$CarInsurance <- as.factor(carinsurance.new$CarInsurance)

#Checking and handling NA data
any(is.na(carinsurance.new)) 
any(is.na(carinsurance.new$Job))
any(is.na(carinsurance.new$ Age))
any(is.na(carinsurance.new$ Marital))
any(is.na(carinsurance.new$ Education))
any(is.na(carinsurance.new$ CarLoan))
any(is.na(carinsurance.new$Balance))
any(is.na(carinsurance.new$Communication))
any(is.na(carinsurance.new$NoOfContacts))
any(is.na(carinsurance.new$CarInsurance))

table(carinsurance.new$Job)
ggplot(carinsurance.new, aes(Job)) + geom_bar()
table(carinsurance.new$Education)
ggplot(carinsurance.new, aes(Education)) + geom_bar()
table(carinsurance.new$Communication)
ggplot(carinsurance.new, aes(Communication)) + geom_bar()

carinsurance.new$Job [is.na(carinsurance.new$Job )] <- 'management'
carinsurance.new$Education [is.na(carinsurance.new$Education)] <- 'secondary'
carinsurance.new$Communication [is.na(carinsurance.new$Communication)]  <- 'cellular'


#coercing
ggplot(carinsurance.new, aes(Age)) + geom_bar()
limit.age <- function(age){
  if (age > 65) return (60)
  return (age)
}
carinsurance.new$Age <- sapply(carinsurance.new$ Age,limit.age)

ggplot(carinsurance.new, aes(NoOfContacts)) + geom_bar()
limit.NoOfContacts <- function(NoOfContacts){
  if (NoOfContacts > 10) return (10)
  return (NoOfContacts)
}
carinsurance.new$NoOfContacts <- sapply(carinsurance.new$NoOfContacts,limit.NoOfContacts)


#divide into trainig set and test set 
filter <- sample.split(carinsurance.new$ CarInsurance, SplitRatio = 0.7)
#Training set 
carInsurance.train <- subset(carinsurance.new, filter==TRUE)
#Test set 
carInsurance.test <- subset(carinsurance.new, filter==F)

dim(carinsurance.new)
dim(carInsurance.train)
dim(carInsurance.test)

#Q3 ----------------------------------------------------------------------------------------------
#Decision Tree Algorithm
model.dt <- rpart(CarInsurance~.,carInsurance.train)
rpart.plot(model.dt , box.palette = "RdBu", shadow.col = "gray", nn = TRUE)
prediction <- predict(model.dt,carInsurance.test)
actual <- carInsurance.test$CarInsurance

#Model random forest
str(carinsurance.new)
model.rf <- randomForest(CarInsurance ~., data = carInsurance.train, importance = TRUE)
prediction.rf <- predict(model.rf,carInsurance.test)

cf.rf <- table(actual,prediction.rf > 0.6)
precision <- cf.rf[1,1]/(cf.rf[1,1] + cf.rf[1,2])
recall <- cf.rf[1,1]/(cf.rf[1,1] + cf.rf[2,1])

#ROC chart 
rocCurveDC <- roc(actual,prediction, direction = ">", levels = c("1", "0"))
rocCurveRF <- roc(actual,prediction.rf, direction = ">", levels = c("1", "0"))

#plot the chart 
plot(rocCurveDC, col = 'red',main = "ROC Chart")
par(new = TRUE)
plot(rocCurveRF, col = 'blue',main = "ROC Chart")

auc(rocCurveDC)
auc(rocCurveRF)

#Q4 ----------------------------------------------------------------------------------------------
numOfcalls <-dim(carinsurance.new)[1] # number of calls we did (4000)
numofYes <- dim(carinsurance.new[carinsurance.new$CarInsurance=='1',])[1] #number of possitive calls (1604)
success_rate <-  numofYes/numOfcalls #success rate is where I call and customer purchase
profit <- success_rate*(200-100)*numOfcalls

#success_rate_rf is the precision of the random forest model 
numofnewcalls <- profit/(precision*(200 - 100))
finalanswer <- numofnewcalls/numOfcalls

importance(model.rf)
varImpPlot(model.rf, main ='Feature importance')
